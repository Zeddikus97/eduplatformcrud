
exports.up = function(knex) {
    return knex.schema.createTable('eduuser', (table)=>{
        table.integer('id').primary().notNullable();
        table.string('name')
        table.string('second_name')
        table.string('password')
        table.string('email')
        table.string('type')
        table.string('social')
    })
    .createTable("module", (table)=>{
        table.integer('id').primary().notNullable();
        table.string('name')
        table.text('description')
        table.string('password')      
        table.integer('amount')
        table.string('currency')
    })
    .createTable("ownership", (table)=>{
        table.integer('id').primary().notNullable();     
        table.integer('user_id')
        .references('id')
        .inTable('eduuser')
        table.integer('module_id')
        .references('id')
        .inTable('module')
        table.string('uuid')
    })
    .createTable("topic", (table)=>{
        table.integer('id').primary().notNullable();     
        table.string('name')
        table.integer('module_id')
        .references('id')
        .inTable('module')
    })
    .createTable("content", (table)=>{
        table.integer('id').primary().notNullable();     
        table.string('type')
        table.text('content')
        table.integer('topic_id')
        .references('id')
        .inTable('topic')
    })
    .createTable("test", (table)=>{
        table.integer('id').primary().notNullable();     
        table.text('content')
        table.integer('module_id')
        .references('id')
        .inTable('module')
    })
};

exports.down = function(knex) { 
    return knex.schema
    .dropTable("test")
    .dropTable("content")
    .dropTable("topic")
    .dropTable("ownership")
    .dropTable("module")
    .dropTable("eduuser")
};
