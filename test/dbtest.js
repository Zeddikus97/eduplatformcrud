const PostgreDB = require('../classes/PostgreDB.js');
const chai = require('chai'); 
const expect = chai.expect;

const postgredb = new PostgreDB('postgres', '5323xrff', 'localhost', '5432', 'eduplatform');

console.log(postgredb)


it('resolvesDBConnection', (done) => {
    postgredb.connectToDB().then( (result) => {
      expect(result).to.equal(true);
    }).finally(done);
})

it('resolvesDBSaveUser', (done) => {
    postgredb.saveUser({name:'Magnus', secondname:'Goodday', password:'4312', email:'magnus@gob.mx', type:'bigboss', social:'face_book'}).then( (result) => {
      console.log(result);
      expect(result).to.equal('success');
    }).finally(done);
})

it('resolvesDBCheckUser', (done) => {
    postgredb.verifyUserPass('magnus@gob.mx','4312').then( (result) => {
      console.log(result);
      expect(result).to.equal(true);
    }).finally(done);
})

it('resolvesDBGetUser', (done) => {
    postgredb.getUserByEmail('magnus@gob.mx').then( (result) => {
      console.log(result);
      expect(result.name).to.equal('Magnus');
    }).finally(done);
})



