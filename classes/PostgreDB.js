class PostgreDB {
    constructor(db_user_name, db_password, ip, port, db_name){
        const express = require('express');
        const { Client } = require('pg');
        this.connectionString = 'postgres://'+db_user_name+':'+db_password+'@'+ip+':'+port+'/'+db_name;
        this.client = new Client({
            connectionString: this.connectionString
        });
    }
    
    connectToDB() {
        return new Promise( (resolve, reject) => this.client.connect(err => {
            if (err) {
                reject(console.error('connection error', err.stack))
            } else {
                resolve(true)
            }
        }))
    }

    connectionDBEnd() {
        this.client.end()
    }

    getOneWithParameters(string_query, params) {
        return new Promise( (resolve, reject) => {
            this.client.query(string_query, params, (err, res) => {
                if (err) {
                    reject(err.stack)
                } else {
                    resolve(res.rows[0])
                }
            })
        })
    }

    insertObject(query_string, params){
        return new Promise( (resolve, reject) => {
            this.client.query(query_string, params, (err, res) => {
                if (err) {
                    reject((err.stack))
                } 
                else {
                    resolve('success');
                }
            })
        })
    }

    getLastIdFromTable(table_name){
        return new Promise( (resolve, reject) => {
            this.client.query('SELECT * FROM ' + table_name + ' ORDER BY id DESC LIMIT 1', (err, res) => {
                if (err) {
                    reject(err.stack)
                } 
                else {
                    if(res.rows.length==0) resolve(0)
                    else resolve(res.rows[0].id)
                }
            })
        })
    }

    verifyUserPass(email, password) {
        console.log(email)
        console.log(password)
        return new Promise( (resolve, reject) => {
            this.client.query('SELECT * FROM eduuser WHERE eduuser.email = ($1)', [email], (err, res) => {
                if (err) {
                    reject(err.stack)
                } 
                else if(res.rows.length==0){
                    resolve(false)
                } else {
                    if (res.rows[0].password==password) {
                        resolve(true)
                    }       
                    resolve('Incorrect password!')         
                }
            })
        })
    }

    async getUserByEmail(email) {
        try{
            //await this.connectToDB();
            return await this.getOneWithParameters('SELECT * FROM eduuser WHERE eduuser.email = ($1)', [email]);
        }
        catch(e){
            console.log(e)
        }
    }


    async verifyingUser(email, password) {
        try{
            //await this.connectToDB();
            const answer = await this.getOneWithParameters(email, password)
            if(!answer) throw new Error("This user doesn't exist!")
            if(answer=='Incorrect password!') throw new Error(answer)
            return answer
        }
        catch(e){
            console.log(e)
        }
    }

    async saveUser(user) {
        try{ 
            //await this.connectToDB();      
            const userver = await this.verifyUserPass(user.email, user.password);
            if(userver) throw new Error("This user already exist!");
            const userid = await this.getLastIdFromTable('eduuser');  
            return await this.insertObject( 'INSERT INTO eduuser VALUES ( ($1), ($2), ($3), ($4), ($5), ($6), ($7) )', [userid+1, user.name, user.secondname, user.password, user.email, user.type, user.social]);       
        }
        catch(e){
            console.log(e)
        }
    }

    /*
    saveNewUser(user) {
        return new Promise( (resolve, reject) => {
            if(this.connectToDB){
                this.client.query('SELECT * FROM startup_user WHERE startup_user.login = ($1)', [login], (err, res) => {
                    if (err) {
                        //this.client.end() 
                        reject((err.stack))
                    } else {
                        resolve(res.rows[0])
                    }
                }
                )
            } 
            else{
                //this.client.end()
                reject('failed connection to db')  
            }
        })
    }
    */

    /*
    connectToDB = () => {
        return new Promise( (resolve, reject) => client.connect(err => {
            if (err) {
                reject(console.error('connection error', err.stack))
            } else {
                resolve(true)
            }
        })
        )
    }

    getUserByLogin = (login) => {
        return new Promise( (resolve, reject) => {
            if(this.connectToDB){
                client.query('SELECT * FROM startup_user WHERE startup_user.login = ($1)', [login], (err, res) => {
                    if (err) {
                        client.end()
                        reject((err.stack))
                    } else {
                        client.end() 
                        resolve(res.rows[0])
                    }
                }
                )
            } 
            else{
                client.end()
                reject('failed connection to db')  
            }
        })
    }*/
}

module.exports = PostgreDB